// ConsoleApplication1.cpp : Defines the entry point for the console application.


#include "stdafx.h"

#pragma comment(lib,"libmx.lib")
#pragma comment(lib,"libmat.lib")
#pragma comment(lib,"libmex.lib")
#pragma comment(lib,"libeng.lib")

#define dlugosc 100

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	// tworzymy maszyne matlaba i ja inicjujemy
	Sleep(100);
	Engine *matlab_engine;
	matlab_engine = engOpen("null");
	mxArray *arr = 0;
	
	// tworzymy okno czybyszewa
	engEvalString(matlab_engine, "okno = chebwin(dlugosc,50);");

	// plik wejsciowy
	ifstream myfile;
	myfile.open("myfile.txt");

	double tmp[dlugosc];
	int i = 0;
	int j = 0;
	double sredni_szum = 0;
	double max_wartosc = 0;
	double stopnie[4];

	// STOP gry
	keybd_event(VkKeyScan(32), 0, 0, 0);
	keybd_event(VkKeyScan(32), 0, KEYEVENTF_KEYUP, 0);


	while (! myfile.eof() ){


		myfile >> tmp[i];
		i++;

		// jezeli nie wczytalismu nowych 100 probek to wczytujemy dalej
		if (i%dlugosc != 0) continue;
		i = 0;
	
		Sleep(400);

		// wysylamy do matlaba dane
		arr = mxCreateDoubleMatrix(1, 100, mxREAL);
		memcpy((void *)mxGetPr(arr), (void *)tmp, sizeof(tmp));
		engPutVariable(matlab_engine, "dane", arr);

		// oknujemy
		engEvalString(matlab_engine, "dane = dane.*okno' ");

		// filtracja gornoprzepustowa
		engEvalString(matlab_engine, "f = [0 0.1 0.1 1]; ");
		engEvalString(matlab_engine, "m = [0 0 1 1]; ");
		engEvalString(matlab_engine, "H = fir2(50,f,m); ");
		engEvalString(matlab_engine, "dane = filter(H,1,dane) ");

		// filtracja dolnoprzepustowa
		engEvalString(matlab_engine, "f = [0 0.8 0.8 1]; ");
		engEvalString(matlab_engine, "m = [1 1 0 0]; ");
		engEvalString(matlab_engine, "H = fir2(50,f,m); ");
		engEvalString(matlab_engine, "dane = filter(H,1,dane) ");


		// filtracja notch na 50Hz
		engEvalString(matlab_engine, "wo = 50/(250); ");   
		engEvalString(matlab_engine, "bw = wo/35; ");
		engEvalString(matlab_engine, "[b,a] = iirnotch(wo,bw); ");
		engEvalString(matlab_engine, "dane = filter(b,a,dane) ");

		
		//liczymy fft
		engEvalString(matlab_engine, "widmo = abs(fft(dane))");
	
		//liczymy moc w widmie
		engEvalString(matlab_engine, "moc = sum(widmo.^2)");

		//wypluwamy wynik:
		arr = engGetVariable(matlab_engine, "moc");
		double e = *(double*)mxGetData(arr);
		cout << e << endl;

		// u�rednianie szumu na poczatku
		if (j < 10){
			sredni_szum += e;
			j++;
			cout << "licze szum " << j << endl;
			continue;
		}
		if (j == 10){
			sredni_szum = sredni_szum / 10;
			cout << "Bierzemy szum �rdni: " << sredni_szum << endl;

			j++;
		}

		// oczekiwanie na pierwszy ruch
		if (max_wartosc == 0){
			if (e > 4 * sredni_szum){
				max_wartosc = e;
				stopnie[0] = sredni_szum + (max_wartosc - sredni_szum) / 4;
				stopnie[1] = sredni_szum + 2 * (max_wartosc - sredni_szum) / 4;
				stopnie[2] = sredni_szum + 3 * (max_wartosc - sredni_szum) / 4;
				stopnie[3] = max_wartosc;
				cout << "Wykryto ruch [START GRY]" << endl;

				keybd_event(VkKeyScan(32), 0, 0, 0);
				keybd_event(VkKeyScan(32), 0, KEYEVENTF_KEYUP, 0);

				// tu mozna wrzucic jakis przycisk startuj�cy gr� np nacisnie spacje czy cos
			}
		
			continue;
		}


		// g�owna detekcja
		if (e > stopnie[0]) {

			if (e > stopnie[1]){

				if (e > stopnie[2]){

					if (e > stopnie[3]){

						cout << "Wykryto ruch: F" << endl;

						keybd_event(VkKeyScan('F'), 0, 0, 0);
						keybd_event(VkKeyScan('F'), 0, KEYEVENTF_KEYUP, 0);
						continue;
					}

					cout << "Wykryto ruch: D" << endl;

					keybd_event(VkKeyScan('D'), 0, 0, 0);
					keybd_event(VkKeyScan('D'), 0, KEYEVENTF_KEYUP, 0);
					continue;
				}

				cout << "Wykryto ruch: S" << endl;

				keybd_event(VkKeyScan('S'), 0, 0, 0);
				keybd_event(VkKeyScan('S'), 0, KEYEVENTF_KEYUP, 0);
				continue;
			}
			cout << "Wykryto ruch: A" << endl;

			keybd_event(VkKeyScan('A'), 0, 0, 0);
			keybd_event(VkKeyScan('A'), 0, KEYEVENTF_KEYUP, 0);
			continue;
		}

		
	}



	// koniec pracy i zamykamy engina
	system("pause");
	engClose(matlab_engine);
	return 0;
}

