﻿using UnityEngine;
using System.Collections;

public class Gracz : MonoBehaviour {

    public GameObject CarBody, Position1, Position2, Position3, Position4;
    public GameManager GM;
    Rigidbody Car;

	void Start () {
        Car = CarBody.GetComponent<Rigidbody>();
	}
	
	void Update () {
        Motion();

	}
    float Acceleration= 5;
    void Motion()
    {

        // bez fizyki
        if (Input.GetKey(KeyCode.A))
        {
            Car.transform.position = Position1.transform.position;
            ChangeOtherLetters(1);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Car.transform.position = Position2.transform.position;
            ChangeOtherLetters(2);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Car.transform.position = Position3.transform.position;
            ChangeOtherLetters(3);
        }

        if (Input.GetKey(KeyCode.F))
        {
            Car.transform.position = Position4.transform.position;
            ChangeOtherLetters(4);
        }



            
        /* Z fizyka
        Vector3 Towards;
        if (Input.GetKey(KeyCode.A))
        {
            Towards = (Position1.transform.position - CarBody.transform.position);
            Car.AddForce(Towards*Acceleration);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Towards = (Position2.transform.position - CarBody.transform.position);
            Car.AddForce(Towards * Acceleration);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Towards = (Position3.transform.position - CarBody.transform.position);
            Car.AddForce(Towards * Acceleration);
        }

        if (Input.GetKey(KeyCode.F))
        {
            Towards = (Position4.transform.position- CarBody.transform.position );
            Car.AddForce(Towards * Acceleration);
        }
        */
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            GM.RestartGame();
        }
    }


    void ChangeOtherLetters(int whichToColor)
    {
        switch(whichToColor)
        {
            case 1:
                Position1.GetComponentInChildren<TextMesh>().color = new Vector4(0, 255, 0, 255);
                Position2.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position3.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position4.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                break;
            case 2:
                Position1.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position2.GetComponentInChildren<TextMesh>().color = new Vector4(0, 255, 0, 255);
                Position3.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position4.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                break;
            case 3:
                Position1.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position2.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position3.GetComponentInChildren<TextMesh>().color = new Vector4(0, 255, 0, 255);
                Position4.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                break;
            case 4:
                Position1.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position2.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                Position4.GetComponentInChildren<TextMesh>().color = new Vector4(0, 255, 0, 255);
                Position3.GetComponentInChildren<TextMesh>().color = new Vector4(255, 255, 255, 255);
                break;
            default:

                break;
        }
    }

}
