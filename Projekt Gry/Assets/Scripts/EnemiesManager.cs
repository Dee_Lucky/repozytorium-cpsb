﻿using UnityEngine;
using System.Collections;

public class EnemiesManager : MonoBehaviour {

    GameObject[] enemies;
    GameObject player;

    public GameObject pickablePrefab;
    public GameObject enemyPrefab;
    public GameObject[] positions;
    public GameManager GM;
    public float everyWhat = 3f;

    public float minDistance = 30f;
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        GenerateEnemies();
    }

    void GenerateEnemies()
    {
        StartCoroutine("CreateEnemies");
    }

    IEnumerator CreateEnemies()
    {
        while(true)
        {
            
            yield return new WaitForSeconds(everyWhat);
            int howManyToCreate = (int)Random.Range(1, 3);
            int position = (int)Random.Range(0, 4);
            if (howManyToCreate == 1)
            {
                Instantiate(enemyPrefab, positions[position].transform.position, new Quaternion(0,0,0,0));
            }
            else
            {

                Instantiate(enemyPrefab, positions[position].transform.position, new Quaternion(0, 0, 0, 0));
                int position2 = (int)Random.Range(0, 4);
                if (position != position2)
                    Instantiate(enemyPrefab, positions[position2].transform.position, positions[position2].transform.rotation);
            }

        }
    }

	
	void Update () {

        AddAll();
        CheckIfFarAway();
        Move();
	}

    void CheckIfFarAway()
    {
        foreach(GameObject enemy in enemies)
        {
            float curdistance = (enemy.transform.position - player.transform.position).magnitude;

            if (curdistance > minDistance)
            {
                DestroyObject(enemy);
                GM.addScorePoint();
            }
        }
    }

    void AddAll()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }
    void Move()
    {
        foreach(GameObject enemy in enemies)
        {
            enemy.transform.position = (enemy.transform.position + new Vector3(-2.5f*Time.deltaTime, 0f, 0f)); 
        }
    }
}
