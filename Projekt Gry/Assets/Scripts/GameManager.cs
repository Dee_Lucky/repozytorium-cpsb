﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {
    
    public static GameManager instance { get; private set; }

    public UnityEngine.UI.Text UIText;
    bool gamePaused;
    int score;
    float lastTimeScale = 1.0f;
    public float howFastToSpeedThingsUp;

    void Awake()
    {
        InitLevel();
    }

    void InitLevel()
    {
        howFastToSpeedThingsUp = 0.2f;
        lastTimeScale = 1.0f;
        score = 0;
        gamePaused = true;

    }

    void PauseGame()
    {

    }

    public void RestartGame()
    {
        Time.timeScale = 1.0f;
        Application.LoadLevel(Application.loadedLevel);
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            Time.timeScale += 0.5f;

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(!gamePaused)
            {
                Debug.Log("Odpauzuj "+ lastTimeScale);
                Time.timeScale = lastTimeScale;
                gamePaused = true;
            }
            else
            {
                Debug.Log("Pauzuj " + lastTimeScale);
                lastTimeScale = Time.timeScale;
                Time.timeScale = 0.0f;
                gamePaused = false;
            }
        }
    }

    void SpeedThingsUp()
    {
        Time.timeScale += howFastToSpeedThingsUp;
    }

    public void addScorePoint()
    {
        ++score;
        string scorre = "" + score;
        UIText.text = "Current Score: " + scorre;
        if (score % 10==0) SpeedThingsUp();
    }


}
