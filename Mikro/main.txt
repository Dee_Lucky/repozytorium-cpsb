/**
*****************************************************************************
**
**  File        : main.c
**
**  Abstract    : main function.
**
**  Functions   : main
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed �as is,� without any warranty
**                of any kind.
**
**  (c)Copyright Atollic AB.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the Atollic TrueSTUDIO(R) toolchain.
**
**
*****************************************************************************
*/

/* Includes */
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_adc.h"
#include "stm32f4xx_exti.h"
#include <string.h>
#include <stdio.h>
#include "stm32f4xx_usart.h"

void NVIC_Config();
void GPIO_Config();
void ADC_Config();

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/
int ConvertedValue = 0; //Converted value readed from ADC

void USART_put(USART_TypeDef* USARTx, uint8_t *s)
{

    while(*s){
        		// wait until data register is empty
    			while( !(USARTx->SR & 0x00000040) );
    			USART_SendData(USARTx, *s);
    			while(USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET)
                {
                }
        		s++;
    		 }
}
void GPIO_USART_Configuration(void)
{

    	   GPIO_InitTypeDef GPIO_InitStructure;
           RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
           RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

         /* Configure USART Tx as alternate function  */
           GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
           GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
           GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

           GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
           GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
           GPIO_Init(GPIOC, &GPIO_InitStructure);

         /* Connect PXx to USARTx_Tx*/
           GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_USART3);
}

void USART3_Configuration(void)
{
    	 	 USART_InitTypeDef             USART_InitStructure;
             USART_InitStructure.USART_BaudRate = 9600;
             USART_InitStructure.USART_WordLength = USART_WordLength_8b;
             USART_InitStructure.USART_StopBits = USART_StopBits_1;
             USART_InitStructure.USART_Parity = USART_Parity_No;
             USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
             USART_InitStructure.USART_Mode = USART_Mode_Tx;

             USART_Init(USART3, &USART_InitStructure);
             USART_Cmd(USART3, ENABLE); // enable USART3
}
void usart_inisialisasi()
     {
         GPIO_USART_Configuration();
         USART3_Configuration();
     }

void adc_configure(){
			ADC_InitTypeDef ADC_init_structure; //Structure for adc confguration
			GPIO_InitTypeDef GPIO_initStructre; //Structure for analog input pin

			//Clock configuration
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);//The ADC1 is connected the APB2 peripheral bus thus we will use its clock source
			RCC_AHB1PeriphClockCmd(RCC_AHB1ENR_GPIOCEN,ENABLE);//Clock for the ADC port

			//Analog pin configuration
			GPIO_initStructre.GPIO_Pin = GPIO_Pin_0;//The channel 10 is connected to PC0
			GPIO_initStructre.GPIO_Mode = GPIO_Mode_AN; //The PC0 pin is configured in analog mode
			GPIO_initStructre.GPIO_PuPd = GPIO_PuPd_NOPULL; //We don't need any pull up or pull down
			GPIO_initStructre.GPIO_Speed 	= GPIO_Speed_50MHz;
			GPIO_Init(GPIOC,&GPIO_initStructre);//Affecting the port with the initialization structure configuration

			//ADC structure configuration
			ADC_DeInit();
			ADC_init_structure.ADC_DataAlign = ADC_DataAlign_Right;//data converted will be shifted to right
			ADC_init_structure.ADC_Resolution = ADC_Resolution_12b;//Input voltage is converted into a 12bit number giving a maximum value of 4096
			ADC_init_structure.ADC_ContinuousConvMode = ENABLE; //the conversion is continuous, the input data is converted more than once
			ADC_init_structure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;// conversion is synchronous with TIM1 and CC1 (actually I'm not sure about this one :/)
			ADC_init_structure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;//no trigger for conversion
			ADC_init_structure.ADC_NbrOfConversion = 1;//I think this one is clear :p
			ADC_init_structure.ADC_ScanConvMode = DISABLE;//The scan is configured in one channel
			ADC_Init(ADC1,&ADC_init_structure);//Initialize ADC with the previous configuration

			//Enable ADC conversion
			ADC_Cmd(ADC1,ENABLE);

			//Select the channel to be read from
			ADC_RegularChannelConfig(ADC1,ADC_Channel_10,1,ADC_SampleTime_144Cycles);
}
int adc_convert(){
		ADC_SoftwareStartConv(ADC1);//Start the conversion
		while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));//Processing the conversion
		return ADC_GetConversionValue(ADC1); //Return the converted data
}
int main(void){
	 uint8_t tmp[2];
	 tmp[1] = 0;
     adc_configure();   //Start configuration
     SystemInit();
     usart_inisialisasi(); //Initialise USART
     while(1){			//loop while the board is working
     tmp[0] = adc_convert()/100;//Read the ADC converted value and divide it by 100 so it is lower than 255
     USART_put(USART3, tmp); //Send the ADC converted value
     int d;   //Delay
     for(d=0;d<2000;d++)
     {}
     }

}







